# Fri kulturs vedtekter

Vedtekter for Fri kultur. Vedtatt av stiftelsesmøtet 14.02.2021

## §0 - Formål

Fri kultur er ein interesseorganisasjon for digitale rettar, digital kultur og etisk teknologi.

Fri Kultur har som formål å:

- Fremme engasjement på digital politikk og etisk teknologi
- Vere ein samarbeidsparter for andre organisasjonar
- Lage eit miljø for kreative mennesker som bruker og fremmer etisk teknologi

Med etisk teknologi meines teknologi som er fri for bruksbegrensningar, med ein åpen
kjeldekode og ikkje utnytter eller overvåker dei som bruke teknologien eller tenesten.

## §1 - Organisatorisk tilknyting

Fri kultur er ein frittståande organisasjon, uta binding til nokon anna organisasjon.

## §2 - Medlemskap

Medlemskap innvilges om ein er over 16 år og betalar medlems-kontingent.

## §3 - Stemmerett og valgbarhet

Alle medlemmar har stemmerett og er valgbare på årsmøtet.

## §4 - Kontingent

Fri kultur har ein medlemskontingent på 150 Kr.
Medlemmar som ikkje har betalt kontingent mister stemmerett og mister medlemskap
dersom kontingent ikkje betales inna eit år.

## §5 - Årsmøte

Årsmøtet er Fri kultur sitt høgste organ og besluttnings-myndigheit. Årsmøtet hendar kvart
år i Februar og Innkalles med minst 2 måneders forvarsel med skriftleg melding til
medlemmane. Frist for oppmelding av sakar er 1 måned før årsmøtet. Fullstendig saksliste må vere tilgjengeleg seinest 3 vekar førårsmøtet.

Alle medlemmar er velkomne til årsmøtet. Årsmøtet og styret kan invitere eksterne-
personar og/eller media til møtet også.

Årsmøtet gjennomfører vedtak med alle medlemmar som er tilstede. Alle medlemmar har
stemmerett og ikkje nokon har meir ein 1 stemme. Om ikkje noko anna er sett skal vedtak
fattes med alminneleg fleirtal. Blanke stemmar føres som ikkje avgitt.

## §6 - Årsmøtets oppgåver

Årsmøtet skal:

- Behandle årsmelding
- Behandle rekneskap
- Behandle innkomne framlegg og sakar
- Vedta budsjett
- Velje styremedlemmar
- Velje valkomité

## §7 - Ekstraordinære-årsmøtar

Ekstraordinære-årsmøtar hendar dersom styret eller 1/3 av medlemmene ber om det.
Møteinnkalling og vedtak hendar etter sama reglar som ordinære årsmøtar.

## §8 - Styret

Fri kultur leies av styret som høgste organ imellom årsmøtane.
Styret skal:

- Gjennomføre årsmøtets vedtak
- Drifte Fri kultur som organisasjon mellom årsmøtene
- Representere Fri kultur utad

Styremøter innkalles av skrivar etter behov. Styret er vedtaksdyktigt så lenge halvparten
av styrets medlemmar er tilstede.

## §9 - Valkomitéen

Valgkomitéen består av leiar og to medlemmar. Man kan kun sitte i valkomitéen 2 år
samanhengende. Valkomitéen har som oppgåve å lage eit heilhetlegt forslag til nytt styre
på årsmøtet.

## §10 - Endringar av vedtektene

Vedtektsendringar kan kun hende under årsmøter eller ekstra-ordinære årsmøtar om det
står i sakslista og kan kun vedtas med 2/3 fleirtal.

## §11 - Oppheving (Kan ikkje endres)

Oppheving av Fri kultur kan berre behandles på ordinært årsmøŧe. Blir oppheving vedtatt
med minst 2/3 fleirtal, innkalles ekstraordinært årsmøte 3 veker seinere. For at oppheving
skal skje må vedtak på 2/3 dels fleirtal gjentas.

